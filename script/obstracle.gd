extends Polygon2D

func _find_sim(node):
	if !node:
		return null
		
	if node is RVOSimulator:
		return node
	return _find_sim(node.get_parent())

func _ready():
	var sim = _find_sim(get_parent())
	
	if sim:
		var poligin = self.polygon
		var tr = self.get_relative_transform_to_parent(sim)
		for i in range(poligin.size()):
			poligin[i] = tr.xform(poligin[i])
				
		sim.add_obstacle(poligin)
