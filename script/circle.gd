extends Node2D

export(Color) var begin_color = Color(1, 0, 0, 1)
export(Color) var end_color =  Color(0, 1, 0, 1)
export(int, 10, 90, 5) var step = 15
export(NodePath) var control_path

var agents = []
var control

func _ready():
	control = get_node(control_path)
	var bar = control.get_node("bar")
	bar.min_value = 10
	bar.max_value = 90
	bar.value = step
	bar.step = 5
	bar.connect("value_changed", self, "_on_bar_value_changed")
	_on_bar_value_changed(step)


func _create_agents():
	var dist = 360
	for a in range(0, 359, step):
		var agent = Agent.new()
		agent.position = Vector2(1, 0).rotated(deg2rad(a)) * (dist + rand_range(10, 20))
		agent.color = begin_color.linear_interpolate(end_color, 1.0 / 360.0 * a)
		var goal = Vector2(1, 0).rotated(deg2rad(a)) * -(dist + rand_range(10, 20))
		agents.append({"agent": agent, "goal": goal})
		agent.name = "agent_%d" % int(a)
		$rvosim/agents.add_child(agent)


func _remove_agents():
	for data in agents:
		data.agent.queue_free()
		data.agent.name = data.agent.name + "_free"
	agents = []


func _process(dt):
	var reached_goal = true
	
	for data in agents:
		var goal_vec: Vector2 = data.goal - data.agent.position

		if goal_vec.length_squared() < 1.0:
			goal_vec = goal_vec.normalized()
		else:
			reached_goal = false

		data.agent.pref_velocity = goal_vec
		
	if reached_goal:
		_update_goals()


func _update_goals():
	for data in agents:
		data.goal *= -1


func _on_reset_pressed():	
	for data in agents:
		data.goal *= -1
		data.agent.reset_position(data.goal * -1)


func _on_bar_value_changed(value):
	control.get_node("step").text = "step %d" % value 
	step = value
	_remove_agents()
	_create_agents()
	
