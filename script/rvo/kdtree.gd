class_name RVOKdTree
extends Reference

class AgentTreeNode:
	var begin : int = -1
	var end : int = -1
	var left : int = -1
	var right : int = -1
	var maxX : float = NAN
	var maxY : float = NAN
	var minX : float = NAN
	var minY : float = NAN
	
	func _init():
		pass
		
	func _print():
		print("begin: %d end: %d" % [begin, end])
		print("left: %d right: %d" % [left, right])
		print("bbox: %f %f %f %f" % [minX, minY, maxX, maxY])
		print("center: %f %f" % [(minX+maxX)*0.5, (minY+maxY)*0.5])


class FloatPair:
	var a : float = NAN
	var b : float = NAN
          
	func _init(a: float, b: float):
		self.a = a
		self.b = b
            
	static func lt(pair1 : FloatPair, pair2 : FloatPair) -> bool: # <
		return pair1.a < pair2.a || !(pair2.a < pair1.a) && pair1.b < pair2.b
 
	static func le(pair1 : FloatPair, pair2 : FloatPair) -> bool:
		return (pair1.a == pair2.a && pair1.b == pair2.b) || FloatPair.lt(pair1, pair2)

	static func gt(pair1 : FloatPair, pair2 : FloatPair) -> bool:  
		return !FloatPair.le(pair1, pair2)

	static func ge(pair1 : FloatPair, pair2 : FloatPair) -> bool:
		return !FloatPair.lt(pair1, pair2)


class ObstacleTreeNode:
	var obstacle : RVOObstacle = null # Obstacle
	var left : ObstacleTreeNode = null # ObstacleTreeNode
	var right : ObstacleTreeNode = null # ObstacleTreeNode
	
	func _init():
		pass


const MAX_LEAF_SIZE : int = 100
var agents = [] # Agent
var agentTree = [] # AgentTreeNode
var obstacleTree = null # ObstacleTreeNode

var simulator

func _init(simulator):
	self.simulator = simulator

func buildAgentTree():
	if (agents == null || agents.size() != simulator.agents.size()):
		agents = simulator.agents.duplicate()
		agentTree = []

		for i in range(2 * agents.size()):
			agentTree.append(AgentTreeNode.new())

	if (!agents.empty()):
		_buildAgentTreeRecursive(0, agents.size(), 0)


func buildObstacleTree():
#	obstacleTree = ObstacleTreeNode.new()
#	print("%d" % simulator.obstacles.size())
	obstacleTree = _buildObstacleTreeRecursive(simulator.obstacles.duplicate())


func computeAgentNeighbors(agent, rangeSq: float): # ref rangeSq
	return _queryAgentTreeRecursive(agent, rangeSq, 0) # ref rangeSq


func computeObstacleNeighbors(agent, rangeSq: float):
	_queryObstacleTreeRecursive(agent, rangeSq, obstacleTree)


func queryVisibility(q1: Vector2, q2: Vector2, radius: float) -> bool:
	return _queryVisibilityRecursive(q1, q2, radius, obstacleTree)


func _buildAgentTreeRecursive( begin: int, end: int, node: int): # private
	agentTree[node].begin = begin
	agentTree[node].end = end
	agentTree[node].minX = agents[begin].position.x
	agentTree[node].minY = agents[begin].position.y
	agentTree[node].maxX = agents[begin].position.x
	agentTree[node].maxY = agents[begin].position.y

	for i in range(begin + 1, end):
		agentTree[node].maxX = max(agentTree[node].maxX, agents[i].position.x)
		agentTree[node].minX = min(agentTree[node].minX, agents[i].position.x)
		agentTree[node].maxY = max(agentTree[node].maxY, agents[i].position.y)
		agentTree[node].minY = min(agentTree[node].minY, agents[i].position.y)

#	print("Node: %d" % node)
#	agentTree[node]._print()
#	print("\n")

	if (end - begin > MAX_LEAF_SIZE):
		var isVertical : bool = agentTree[node].maxX - agentTree[node].minX > agentTree[node].maxY - agentTree[node].minY
		var splitValue : float = 0.5 * ((agentTree[node].maxX + agentTree[node].minX) if isVertical else (agentTree[node].maxY + agentTree[node].minY))
		
		var left : int = begin
		var right : int = end

		while (left < right):
			while left < right && (agents[left].position.x if isVertical else agents[left].position.y) < splitValue:
				left += 1

			while right > left && (agents[right - 1].position.x if isVertical else agents[right - 1].position.y) >= splitValue:
				right -= 1

			if left < right:
				var tempAgent = agents[left]
				agents[left] = agents[right - 1]
				agents[right - 1] = tempAgent
				left += 1
				right -= 1

		var leftSize : int = left - begin

		if leftSize == 0:
			leftSize += 1
			left += 1
			right += 1

		agentTree[node].left = node + 1
		agentTree[node].right = node + 2 * leftSize

		_buildAgentTreeRecursive(begin, left, agentTree[node].left)
		_buildAgentTreeRecursive(left, end, agentTree[node].right)


func _buildObstacleTreeRecursive(obstacles) -> ObstacleTreeNode : # private ObstacleTreeNode
	if (obstacles.empty()):
		return null

	var node = ObstacleTreeNode.new()

	var optimalSplit : int = 0
	var minLeft : int = obstacles.size()
	var minRight : int = obstacles.size()

	for i in range(obstacles.size()):
		var leftSize : int = 0
		var rightSize : int = 0

		var obstacleI1 : RVOObstacle = obstacles[i]
		var obstacleI2 : RVOObstacle = obstacleI1.next

		for j in range(obstacles.size()):
			if i == j:
				continue

			var obstacleJ1 : RVOObstacle = obstacles[j]
			var obstacleJ2 : RVOObstacle = obstacleJ1.next

			var j1LeftOfI : float = RVOMath.leftOf(obstacleI1.point, obstacleI2.point, obstacleJ1.point)
			var j2LeftOfI : float = RVOMath.leftOf(obstacleI1.point, obstacleI2.point, obstacleJ2.point)

			if j1LeftOfI >= -RVOMath.RVO_EPSILON && j2LeftOfI >= -RVOMath.RVO_EPSILON:
				leftSize += 1
			elif j1LeftOfI <= RVOMath.RVO_EPSILON && j2LeftOfI <= RVOMath.RVO_EPSILON:
 				rightSize += 1
			else:
				leftSize += 1
				rightSize += 1

			var pairl = FloatPair.new(max(leftSize, rightSize), min(leftSize, rightSize))
			var pairr = FloatPair.new(max(minLeft, minRight), min(minLeft, minRight))
			if FloatPair.ge(pairl, pairr):
				break

		var pairl = FloatPair.new(max(leftSize, rightSize), min(leftSize, rightSize))
		var pairr = FloatPair.new(max(minLeft, minRight), min(minLeft, minRight))
		if FloatPair.lt(pairl, pairr):
			minLeft = leftSize
			minRight = rightSize
			optimalSplit = i
   
	var leftObstacles = []
	leftObstacles.resize(minLeft)

	var rightObstacles = []
	rightObstacles.resize(minRight)

	var leftCounter : int = 0
	var rightCounter : int = 0
	var i : int = optimalSplit

	var obstacleI1 : RVOObstacle = obstacles[i]
	var obstacleI2 : RVOObstacle = obstacleI1.next

	for j in range(obstacles.size()):
		if i == j:
			continue

		var obstacleJ1 : RVOObstacle = obstacles[j]
		var obstacleJ2 : RVOObstacle = obstacleJ1.next

		var j1LeftOfI : float = RVOMath.leftOf(obstacleI1.point, obstacleI2.point, obstacleJ1.point)
		var j2LeftOfI : float = RVOMath.leftOf(obstacleI1.point, obstacleI2.point, obstacleJ2.point)

		if j1LeftOfI >= -RVOMath.RVO_EPSILON && j2LeftOfI >= -RVOMath.RVO_EPSILON:
			leftObstacles[leftCounter] = obstacles[j]
			leftCounter += 1
		elif j1LeftOfI <= RVOMath.RVO_EPSILON && j2LeftOfI <= RVOMath.RVO_EPSILON:
			rightObstacles[rightCounter] = obstacles[j]
			rightCounter += 1
		else:
			var t : float = (obstacleI2.point - obstacleI1.point).cross(obstacleJ1.point - obstacleI1.point) / (obstacleI2.point - obstacleI1.point).cross(obstacleJ1.point - obstacleJ2.point)
			var splitPoint : Vector2 = obstacleJ1.point + t * (obstacleJ2.point - obstacleJ1.point)

			var newObstacle = RVOObstacle.new()
			newObstacle.point = splitPoint
			newObstacle.previous = obstacleJ1
			newObstacle.next = obstacleJ2
			newObstacle.convex = true
			newObstacle.direction = obstacleJ1.direction

			newObstacle.id = simulator.obstacles.size()

			simulator.obstacles.append(newObstacle)

			obstacleJ1.next = newObstacle
			obstacleJ2.previous = newObstacle

			if j1LeftOfI > 0.0:
				leftObstacles[leftCounter] = obstacleJ1
				leftCounter += 1
				rightObstacles[rightCounter] = newObstacle
				rightCounter += 1
			else:
				rightObstacles[rightCounter] = obstacleJ1
				rightCounter += 1
				leftObstacles[leftCounter] = newObstacle
				leftCounter += 1

	node.obstacle = obstacleI1
	node.left = _buildObstacleTreeRecursive(leftObstacles)
	node.right = _buildObstacleTreeRecursive(rightObstacles)

	return node

 
func _queryAgentTreeRecursive(agent, rangeSq: float, node: int): # private, ref rangeSq
	if agentTree[node].end - agentTree[node].begin <= MAX_LEAF_SIZE:
		for i in range(agentTree[node].begin,  agentTree[node].end):
			rangeSq = agent.insertAgentNeighbor(agents[i], rangeSq) # ref rangeSq
	else:
		var distSqLeft : float = pow(max(0.0, agentTree[agentTree[node].left].minX - agent.position.x), 2) + pow(max(0.0, agent.position.x - agentTree[agentTree[node].left].maxX), 2) + pow(max(0.0, agentTree[agentTree[node].left].minY - agent.position.y), 2) + pow(max(0.0, agent.position.y - agentTree[agentTree[node].left].maxY), 2)
		var distSqRight : float = pow(max(0.0, agentTree[agentTree[node].right].minX - agent.position.x), 2) + pow(max(0.0, agent.position.x - agentTree[agentTree[node].right].maxX), 2) + pow(max(0.0, agentTree[agentTree[node].right].minY - agent.position.y), 2) + pow(max(0.0, agent.position.y - agentTree[agentTree[node].right].maxY),2)

		if distSqLeft < distSqRight:
			if distSqLeft < rangeSq:
				rangeSq = _queryAgentTreeRecursive(agent, rangeSq, agentTree[node].left) # ref rangeSq

				if distSqRight < rangeSq:
					rangeSq = _queryAgentTreeRecursive(agent, rangeSq, agentTree[node].right) # ref rangeSq
		else:
			if distSqRight < rangeSq:
				rangeSq = _queryAgentTreeRecursive(agent, rangeSq, agentTree[node].right) # ref rangeSq

				if distSqLeft < rangeSq:
					rangeSq = _queryAgentTreeRecursive(agent, rangeSq, agentTree[node].left) # ref rangeSq
	return rangeSq

   
func _queryObstacleTreeRecursive(agent, rangeSq: float, node: ObstacleTreeNode): # private
	if node:
		var obstacle1 : RVOObstacle = node.obstacle
		var obstacle2 : RVOObstacle = obstacle1.next

		var agentLeftOfLine : float = RVOMath.leftOf(obstacle1.point, obstacle2.point, agent.position)

		_queryObstacleTreeRecursive(agent, rangeSq, node.left if agentLeftOfLine >= 0.0 else node.right)

		var div = (obstacle2.point - obstacle1.point).length_squared() # тут тоже деление на ноль
		var distSqLine : float = pow(agentLeftOfLine, 2) / div if div != 0 else 0

		if distSqLine < rangeSq:
			if agentLeftOfLine < 0.0:
				agent.insertObstacleNeighbor(node.obstacle, rangeSq)
			
			_queryObstacleTreeRecursive(agent, rangeSq, node.right if agentLeftOfLine >= 0.0 else node.left)

 
func _queryVisibilityRecursive(q1: Vector2, q2: Vector2, radius: float, node: ObstacleTreeNode) -> bool: # private
	if !node:
		return true

	var obstacle1 : RVOObstacle = node.obstacle
	var obstacle2 : RVOObstacle = obstacle1.next

	var q1LeftOfI : float = RVOMath.leftOf(obstacle1.point, obstacle2.point, q1)
	var q2LeftOfI : float = RVOMath.leftOf(obstacle1.point, obstacle2.point, q2)
	var invLengthI : float = 1.0 / (obstacle2.point - obstacle1.point).length_squared()

	if q1LeftOfI >= 0.0 && q2LeftOfI >= 0.0:
		return _queryVisibilityRecursive(q1, q2, radius, node.left) && ((pow(q1LeftOfI, 2) * invLengthI >= pow(radius, 2) && pow(q2LeftOfI, 2) * invLengthI >= pow(radius, 2)) || _queryVisibilityRecursive(q1, q2, radius, node.right))

	if q1LeftOfI <= 0.0 && q2LeftOfI <= 0.0:
		return _queryVisibilityRecursive(q1, q2, radius, node.right) && ((pow(q1LeftOfI, 2) * invLengthI >= pow(radius, 2) && pow(q2LeftOfI, 2) * invLengthI >= pow(radius, 2)) || _queryVisibilityRecursive(q1, q2, radius, node.left))

	if q1LeftOfI >= 0.0 && q2LeftOfI <= 0.0:
		return _queryVisibilityRecursive(q1, q2, radius, node.left) && _queryVisibilityRecursive(q1, q2, radius, node.right)

	var point1LeftOfQ : float = RVOMath.leftOf(q1, q2, obstacle1.point)
	var point2LeftOfQ : float = RVOMath.leftOf(q1, q2, obstacle2.point)
	var invLengthQ : float = 1.0 / (q2 - q1).length_squared()

	return point1LeftOfQ * point2LeftOfQ >= 0.0 && pow(point1LeftOfQ, 2) * invLengthQ > pow(radius, 2) && pow(point2LeftOfQ, 2) * invLengthQ > pow(radius, 2) && _queryVisibilityRecursive(q1, q2, radius, node.left) && _queryVisibilityRecursive(q1, q2, radius, node.right)
