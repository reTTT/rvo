class_name RVOSimulator
extends Node2D

var agents = []
var obstacles = []
var kdTree
var timeStep = 1.0 / 30.0

var globalTime : float = 0
var need_build_Obstacle = false

func _addAgent(agent):
	agents.append(agent)
	

func add_obstacle(vertices):
	if (vertices.size() < 2):
		return -1

	var obstacleNo : int = obstacles.size()

	for i in range(vertices.size()):
		var obstacle : RVOObstacle = RVOObstacle.new()
		obstacle.point = vertices[i]

		if i != 0:
			obstacle.previous = obstacles[obstacles.size() - 1]
			obstacle.previous.next = obstacle

		if i == vertices.size() - 1:
			obstacle.next = obstacles[obstacleNo]
			obstacle.next.previous = obstacle

		obstacle.direction = (vertices[(0 if i == vertices.size() - 1 else i + 1)] - vertices[i]).normalized()

		if vertices.size() == 2:
			obstacle.convex = true
		else:
			obstacle.convex = (RVOMath.leftOf(vertices[(vertices.size() - 1 if i == 0 else i - 1)], vertices[i], vertices[(0 if i == vertices.size() - 1 else i + 1)]) >= 0.0)

		obstacle.id = obstacles.size()
		obstacles.append(obstacle)
		
	need_build_Obstacle = true

	return obstacleNo

	#neighborDist: 15.0f, maxNeighbors: 10, timeHorizon: 10, timeHorizonObst: 10.0f, radius: 1.5f, maxSpeed: 2.0f, velocity: Vector2(0.0f, 0.0f)
#float , int , float , float , float , float , Vector2 velocity
func create_agent(pos: Vector2, radius: float, maxSpeed: float):
	var agent = RVOAgent.new(self)
	agent.position = pos
	agent.velocity = Vector2(0, 0)
#	agent.id = RID
	agent.maxNeighbors = 10 # +
	agent.maxSpeed = maxSpeed # +
	agent.neighborDist = radius * 6 # +
	agent.radius = radius
	agent.timeHorizon = 10.0 # +
	agent.timeHorizonObst = 10.0 # +
	_addAgent(agent)
	return agent

func remove_agent(agent):
	agents.erase(agent)

func _ready():
	kdTree = RVOKdTree.new(self)
	kdTree.buildObstacleTree()


func _doStep(dt) -> float:
	if need_build_Obstacle:
		need_build_Obstacle = false
		kdTree.buildObstacleTree()
	
	kdTree.buildAgentTree()

	for agent in agents:
		agent.computeNeighbors()
		agent.computeNewVelocity()

	for agent in agents:
		agent.update()
	
	timeStep = dt
	globalTime += dt

	return globalTime


func processObstacles():
	kdTree.buildObstacleTree()


#func _physics_process(dt):
func _process(dt):
	_doStep(dt)
