class_name RVOMath
extends Reference

const RVO_EPSILON = 0.00001

#static func det(v1 : Vector2, v2 : Vector2) -> float:
#	return v1.cross(v2)


static func leftOf(a: Vector2, b: Vector2, c: Vector2) -> float:
	return (a - c).cross(b - a)

static func distSqPointLineSegment(vector1: Vector2, vector2: Vector2, vector3: Vector2) -> float:
	var r : float = (vector3 - vector1).dot(vector2 - vector1) / (vector2 - vector1).length_squared()

	if r < 0.0:
		return (vector3 - vector1).length_squared()

	if r > 1.0:
		return (vector3 - vector2).length_squared()

	return (vector3 - (vector1 + r * (vector2 - vector1))).length_squared()