class_name RVOAgent
extends Reference

class KeyValuePair:
	var key
	var value
	
	func _init(key, value):
		self.key = key
		self.value = value


const RVO_EPSILON = 0.00001

var agentNeighbors = [] # new List<KeyValuePair<float, Agent>>();
var obstacleNeighbors = [] # new List<KeyValuePair<float, Obstacle>>();
var orcaLines = [] # new List<Line>();
var position : Vector2 = Vector2() # +
var prefVelocity : Vector2 = Vector2()
var velocity : Vector2 = Vector2() # +
var id : int = 0
var maxNeighbors : int = 0 # +
var maxSpeed : float = 0.0 # +
var neighborDist : float = 0.0 # +
var radius : float = 0.0 # +
var timeHorizon : float = 0.0 # +
var timeHorizonObst : float = 0.0 # +

var newVelocity : Vector2 = Vector2() # privat

var simulator

func _init(simulator):
	self.simulator = simulator


class ObstacleSort:
	static func sort(a, b):
		return a.key < b.key

        
func computeNeighbors():
	obstacleNeighbors.clear()
	var rangeSq : float = pow(timeHorizonObst * maxSpeed + radius, 2)
	simulator.kdTree.computeObstacleNeighbors(self, rangeSq)

	obstacleNeighbors.sort_custom(ObstacleSort, "sort")

	agentNeighbors.clear()

	if maxNeighbors > 0:
		rangeSq = pow(neighborDist, 2)
		rangeSq = simulator.kdTree.computeAgentNeighbors(self, rangeSq) # ref 2 par


func computeNewVelocity():
	orcaLines.clear()

	var invTimeHorizonObst : float = 1.0 / timeHorizonObst
         
	for i in range(obstacleNeighbors.size()):
		var obstacle1 = obstacleNeighbors[i].value
		var obstacle2 = obstacle1.next
		var relativePosition1 : Vector2 = obstacle1.point - position
		var relativePosition2 : Vector2 = obstacle2.point - position
		var alreadyCovered : bool = false

		for j in range(orcaLines.size()):
			if (invTimeHorizonObst * relativePosition1 - orcaLines[j].point).cross(orcaLines[j].direction) - invTimeHorizonObst * radius >= -RVO_EPSILON && (invTimeHorizonObst * relativePosition2 - orcaLines[j].point).cross(orcaLines[j].direction) - invTimeHorizonObst * radius >= -RVO_EPSILON:
				alreadyCovered = true
				break

		if (alreadyCovered):
			continue

		var distSq1 : float = relativePosition1.length_squared()
		var distSq2 : float = relativePosition2.length_squared()
		var radiusSq : float = pow(radius, 2)
		var obstacleVector : Vector2 = obstacle2.point - obstacle1.point
		var s : float = (relativePosition1 * -1).dot(obstacleVector) / obstacleVector.length_squared()
		var tmp : Vector2 = (-relativePosition1 - s * obstacleVector)
		var distSqLine : float = tmp.length_squared()
		var line = Line.new()

		if (s < 0.0 && distSq1 <= radiusSq):
			if (obstacle1.convex):
				line.point = Vector2(0.0, 0.0)
				line.direction = Vector2(-relativePosition1.y, relativePosition1.x).normalized()
				orcaLines.append(line)

			continue
		elif (s > 1.0 && distSq2 <= radiusSq):
			if (obstacle2.convex && relativePosition2.cross(obstacle2.direction) >= 0.0):
				line.point = Vector2(0.0, 0.0)
				line.direction = Vector2(-relativePosition2.y, relativePosition2.x).normalized()
				orcaLines.append(line)

			continue
		elif (s >= 0.0 && s < 1.0 && distSqLine <= radiusSq):
			line.point = Vector2(0.0, 0.0)
			line.direction = -obstacle1.direction
			orcaLines.append(line)
			continue

		var leftLegDirection : Vector2 
		var rightLegDirection : Vector2

		if (s < 0.0 && distSqLine <= radiusSq):
			if !obstacle1.convex:
				continue

			obstacle2 = obstacle1
			var leg1 : float = sqrt(distSq1 - radiusSq)
			leftLegDirection = Vector2(relativePosition1.x * leg1 - relativePosition1.y * radius, relativePosition1.x * radius + relativePosition1.y * leg1) / distSq1
			rightLegDirection = Vector2(relativePosition1.x * leg1 + relativePosition1.y * radius, -relativePosition1.x * radius + relativePosition1.y * leg1) / distSq1

		elif (s > 1.0 && distSqLine <= radiusSq):
			if (!obstacle2.convex):
				continue

			obstacle1 = obstacle2
			var leg2 : float = sqrt(distSq2 - radiusSq)
			leftLegDirection = Vector2(relativePosition2.x * leg2 - relativePosition2.y * radius, relativePosition2.x * radius + relativePosition2.y * leg2) / distSq2
			rightLegDirection = Vector2(relativePosition2.x * leg2 + relativePosition2.y * radius, -relativePosition2.x * radius + relativePosition2.y * leg2) / distSq2
		else:
			if obstacle1.convex:
				var leg1 : float = sqrt(distSq1 - radiusSq)
				leftLegDirection = Vector2(relativePosition1.x * leg1 - relativePosition1.y * radius, relativePosition1.x * radius + relativePosition1.y * leg1) / distSq1
			else: 
				leftLegDirection = -obstacle1.direction;

			if (obstacle2.convex):
				var leg2 : float = sqrt(distSq2 - radiusSq)
				rightLegDirection = Vector2(relativePosition2.x * leg2 + relativePosition2.y * radius, -relativePosition2.x * radius + relativePosition2.y * leg2) / distSq2
			else:
				rightLegDirection = obstacle1.direction

		var leftNeighbor = obstacle1.previous

		var isLeftLegForeign : bool = false
		var isRightLegForeign : bool = false

		if (obstacle1.convex && leftLegDirection.cross(-leftNeighbor.direction) >= 0.0):
			leftLegDirection = -leftNeighbor.direction
			isLeftLegForeign = true

		if (obstacle2.convex && rightLegDirection.cross(obstacle2.direction) <= 0.0):
			rightLegDirection = obstacle2.direction
			isRightLegForeign = true
                
		var leftCutOff : Vector2 = invTimeHorizonObst * (obstacle1.point - position)
		var rightCutOff : Vector2 = invTimeHorizonObst * (obstacle2.point - position)
		var cutOffVector : Vector2  = rightCutOff - leftCutOff
                
		var t : float = 0.5
		
		if obstacle1 != obstacle2:
			if cutOffVector.length_squared() > 0: # хрень какая то. что бы небыло деления на ноль
				t = (velocity - leftCutOff).dot(cutOffVector) / cutOffVector.length_squared()
			else:
				t = 0
		
		var tLeft : float = (velocity - leftCutOff).dot(leftLegDirection);
		var tRight : float = (velocity - rightCutOff).dot(rightLegDirection);

		if ((t < 0.0 && tLeft < 0.0) || (obstacle1 == obstacle2 && tLeft < 0.0 && tRight < 0.0)):
			var unitW : Vector2 = (velocity - leftCutOff).normalized()
			line.direction = Vector2(unitW.y, -unitW.x)
			line.point = leftCutOff + radius * invTimeHorizonObst * unitW
			orcaLines.append(line)
			continue
		elif (t > 1.0 && tRight < 0.0):
			var unitW : Vector2 = (velocity - rightCutOff).normalized()
			line.direction = Vector2(unitW.y, -unitW.x)
			line.point = rightCutOff + radius * invTimeHorizonObst * unitW
			orcaLines.append(line)
			continue
                
		var tmp1 : Vector2 = velocity - (leftCutOff + t * cutOffVector)
		var distSqCutoff : float = tmp1.length_squared()
		
		if (t < 0.0 || t > 1.0 || obstacle1 == obstacle2):
			 distSqCutoff = INF # float.PositiveInfinity

		var distSqLeft : float = (velocity - (leftCutOff + tLeft * leftLegDirection)).length_squared()
		
		if tLeft < 0.0:
			distSqLeft = INF # float.PositiveInfinity
		
		var distSqRight : float = (velocity - (rightCutOff + tRight * rightLegDirection)).length_squared()
		
		if tRight < 0.0: 
			distSqRight = INF #float.PositiveInfinity
		
		if (distSqCutoff <= distSqLeft && distSqCutoff <= distSqRight):
			line.direction = -obstacle1.direction
			line.point = leftCutOff + radius * invTimeHorizonObst * Vector2(-line.direction.y, line.direction.x)
			orcaLines.append(line)
			continue

		if (distSqLeft <= distSqRight):
			if (isLeftLegForeign):
				continue

			line.direction = leftLegDirection
			line.point = leftCutOff + radius * invTimeHorizonObst * Vector2(-line.direction.y, line.direction.x)
			orcaLines.append(line)
			continue
                
		if (isRightLegForeign):
			continue

		line.direction = -rightLegDirection
		line.point = rightCutOff + radius * invTimeHorizonObst * Vector2(-line.direction.y, line.direction.x)
		orcaLines.append(line)

	var numObstLines : int = orcaLines.size()
	var invTimeHorizon : float = 1.0 / timeHorizon
            
	for i in range(agentNeighbors.size()):
		var other = agentNeighbors[i].value

		var relativePosition : Vector2 = other.position - position
		var relativeVelocity : Vector2 = velocity - other.velocity
		var distSq : float = relativePosition.length_squared()
		var combinedRadius : float = radius + other.radius
		var combinedRadiusSq : float = pow(combinedRadius, 2)

		var line = Line.new()
		var u : Vector2 = Vector2()

		if (distSq > combinedRadiusSq):          
			var w : Vector2 = relativeVelocity - invTimeHorizon * relativePosition    
			var wLengthSq : float = w.length_squared()
			var dotProduct1 : float = w.dot(relativePosition)

			if (dotProduct1 < 0.0 && pow(dotProduct1, 2) > combinedRadiusSq * wLengthSq):
				var wLength : float = sqrt(wLengthSq)
				var unitW : Vector2 = w / wLength

				line.direction = Vector2(unitW.y, -unitW.x)
				u = (combinedRadius * invTimeHorizon - wLength) * unitW
			else:        
				var leg : float = sqrt(distSq - combinedRadiusSq)

				if relativePosition.cross(w) > 0.0:   
					line.direction = Vector2(relativePosition.x * leg - relativePosition.y * combinedRadius, relativePosition.x * combinedRadius + relativePosition.y * leg) / distSq
				else:
					line.direction = -Vector2(relativePosition.x * leg + relativePosition.y * combinedRadius, -relativePosition.x * combinedRadius + relativePosition.y * leg) / distSq

				var dotProduct2 : float = relativeVelocity * line.direction
				u = dotProduct2 * line.direction - relativeVelocity
		else:   
			var invTimeStep : float = 1.0 / simulator.timeStep;
			var w : Vector2 = relativeVelocity - invTimeStep * relativePosition
			var wLength : float = w.length()
			var unitW : Vector2 = w / wLength
			line.direction = Vector2(unitW.y, -unitW.x)
			u = (combinedRadius * invTimeStep - wLength) * unitW

		line.point = velocity + 0.5 * u
		orcaLines.append(line)

	var r = linearProgram2(orcaLines, maxSpeed, prefVelocity, false, newVelocity)
	newVelocity = r[1]
	var lineFail : int = r[0] # ref newVelocity

	if (lineFail < orcaLines.size()):
		newVelocity = linearProgram3(orcaLines, numObstLines, lineFail, maxSpeed, newVelocity) # ref newVelocity

        
func ___insertAgentNeighbor(agent, rangeSq : float): # ref rangeSq : float
	if self != agent:
		var distSq : float = (position - agent.position).length_squared()

		if distSq < rangeSq:
			if agentNeighbors.size() < maxNeighbors:
				agentNeighbors.append(KeyValuePair.new(distSq, agent))

			var i : int = agentNeighbors.size() - 1

			while (i != 0 && distSq < agentNeighbors[i - 1].key):
				agentNeighbors[i] = agentNeighbors[i - 1]
				i -= 1

			agentNeighbors[i] = KeyValuePair.new(distSq, agent)

			if agentNeighbors.size() == maxNeighbors:
				rangeSq = agentNeighbors[agentNeighbors.size() - 1].key
				
	return rangeSq


func insertAgentNeighbor(agent, rangeSq : float): # ref rangeSq : float
	if self != agent:
		var distSq : float = (position - agent.position).length_squared()

		if distSq < rangeSq:
			if agentNeighbors.size() < maxNeighbors:
				var i : int = agentNeighbors.size()

				while (i != 0 && distSq < agentNeighbors[i - 1].key):
					i -= 1

				agentNeighbors.insert(i, KeyValuePair.new(distSq, agent))

			if agentNeighbors.size() == maxNeighbors:
				rangeSq = agentNeighbors.back().key
				
	return rangeSq


func ___insertObstacleNeighbor(obstacle, rangeSq : float):
	var nextObstacle = obstacle.next;

	var distSq : float = RVOMath.distSqPointLineSegment(obstacle.point, nextObstacle.point, position);

	if distSq < rangeSq:
		obstacleNeighbors.append(KeyValuePair.new(distSq, obstacle))

		var i :int = obstacleNeighbors.size() - 1;
		# вставка с сортировкой, сартировку можно сделать позже а тут просто вставку
		while i != 0 && distSq < obstacleNeighbors[i - 1].key:
			obstacleNeighbors[i] = obstacleNeighbors[i - 1]
			i -= 1
		
		obstacleNeighbors[i] = KeyValuePair.new(distSq, obstacle)


func insertObstacleNeighbor(obstacle, rangeSq : float):
	var nextObstacle = obstacle.next;

	var distSq : float = RVOMath.distSqPointLineSegment(obstacle.point, nextObstacle.point, position);

	if distSq < rangeSq:
		obstacleNeighbors.append(KeyValuePair.new(distSq, obstacle))


func _____insertObstacleNeighbor(obstacle, rangeSq : float):
	var nextObstacle = obstacle.next;

	var distSq : float = RVOMath.distSqPointLineSegment(obstacle.point, nextObstacle.point, position);

	if distSq < rangeSq:
		var i :int = obstacleNeighbors.size()
		# вставка с сортировкой, сартировку можно сделать позже а тут просто вставку
		while i != 0 && distSq < obstacleNeighbors[i-1].key:
			i -= 1
		
		obstacleNeighbors.insert(i, KeyValuePair.new(distSq, obstacle))
			
#		for t in obstacleNeighbors:
#			print(str(t.key))
				
#		print("end")
#		print("\n")
        
func update():
	velocity = newVelocity
	position += velocity * simulator.timeStep

        
func linearProgram1(lines, lineNo: int, radius: float, optVelocity: Vector2, directionOpt: bool, result: Vector2): # private, ref result: Vector2
	var dotProduct : float = lines[lineNo].point.dot(lines[lineNo].direction)
	var discriminant : float = pow(dotProduct, 2) + pow(radius, 2) - lines[lineNo].point.length_squared()

	if discriminant < 0.0:          
		return [false, result]

	var sqrtDiscriminant : float = sqrt(discriminant);
	var tLeft : float = -dotProduct - sqrtDiscriminant;
	var tRight : float = -dotProduct + sqrtDiscriminant;

	for i in range(lineNo):
		var denominator : float = lines[lineNo].direction.cross(lines[i].direction)
		var numerator : float  = lines[i].direction.cross(lines[lineNo].point - lines[i].point)

		if abs(denominator) <= RVO_EPSILON:
			if numerator < 0.0:
				return [false, result]

			continue

		var t : float = numerator / denominator

		if denominator >= 0.0:
			tRight = min(tRight, t)
		else:
			tLeft = max(tLeft, t)

		if tLeft > tRight:
			return [false, result]

	if directionOpt:
		if optVelocity.dot(lines[lineNo].direction) > 0.0:
			result = lines[lineNo].point + tRight * lines[lineNo].direction
		else:
			result = lines[lineNo].point + tLeft * lines[lineNo].direction
	else:
		var t : float = lines[lineNo].direction.dot(optVelocity - lines[lineNo].point)

		if t < tLeft:
			result = lines[lineNo].point + tLeft * lines[lineNo].direction
		elif t > tRight:
			result = lines[lineNo].point + tRight * lines[lineNo].direction
		else:
			result = lines[lineNo].point + t * lines[lineNo].direction

	return [true, result]

        
func linearProgram2(lines, radius: float, optVelocity: Vector2, directionOpt: bool, result: Vector2): # private ref result: Vector2
	if directionOpt:
		result = optVelocity * radius
	elif optVelocity.length_squared() > pow(radius, 2):
		result = optVelocity.normalized() * radius
	else:
		result = optVelocity

	for i in range(lines.size()):
		if lines[i].direction.cross(lines[i].point - result) > 0.0:
			var tempResult : Vector2 = result
			var r = linearProgram1(lines, i, radius, optVelocity, directionOpt, result)
			result = r[1]
			if !r[0]: # ref result
				result = tempResult
				return [i, result]

	return [lines.size(), result]

        
func linearProgram3(lines, numObstLines : int, beginLine : int, radius : float, result : Vector2 ) -> Vector2: # private
	var distance : float = 0.0

	for i in range(beginLine, lines.size()):
		if lines[i].direction.cross(lines[i].point - result) > distance:
			var projLines = [] # new List<Line>();
			
			for ii in range(numObstLines):
				projLines.append(lines[ii])

			for j in  range(numObstLines, i):
				var line = Line.new()
				var determinant : float = lines[i].direction.cross(lines[j].direction)

				if abs(determinant) <= RVO_EPSILON:
					if lines[i].direction.dot(lines[j].direction) > 0.0:
						continue
					else:
						line.point = 0.5 * (lines[i].point + lines[j].point);
				else:
					line.point = lines[i].point + (lines[j].direction.cross(lines[i].point - lines[j].point) / determinant) * lines[i].direction
				
				line.direction = (lines[j].direction - lines[i].direction).normalized()
				projLines.append(line)

			var tempResult : Vector2 = result
			
			var r = linearProgram2(projLines, radius, Vector2(-lines[i].direction.y, lines[i].direction.x), true, result)
			result = r[1]
			if r[0] < projLines.size(): # ref result
				result = tempResult

			distance = lines[i].direction.cross(lines[i].point - result)
	
	return result
