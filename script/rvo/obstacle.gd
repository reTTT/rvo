class_name RVOObstacle
extends Reference

var next # Obstacle
var previous # Obstacle
var direction : Vector2 = Vector2()
var point : Vector2 = Vector2()
var id : int = 0
var convex : bool = false