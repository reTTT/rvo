class_name Agent
extends Node2D

export(Color) var color = Color(1, 1, 0, 1)
export(float) var radius = 20
export(float) var max_speed = 120

var rvoagent

var pref_velocity: Vector2 = Vector2(0, 0) setget set_pref_velocity, get_pref_velocity

func _find_sim(node):
	if !node:
		return null
		
	if node is RVOSimulator:
		return node
	return _find_sim(node.get_parent())
	
func _ready():
	var sim = _find_sim(get_parent())
	if sim:
		rvoagent = sim.create_agent(position, radius, max_speed)

func _exit_tree():
	var sim = _find_sim(get_parent())
	if sim:
		sim.remove_agent(rvoagent)

func _process(delta):
	if rvoagent:
		self.position = rvoagent.position
		update()

func _draw():
	draw_circle(Vector2(0, 0), radius + 1, Color(0, 0, 0, 0.5))
	draw_circle(Vector2(0, 0), radius, color)
	var vel_color = color.inverted()
	vel_color.a = 1
	draw_line(Vector2(0, 0),  rvoagent.velocity, vel_color)


func set_pref_velocity(vel: Vector2):
	if rvoagent:
		rvoagent.prefVelocity = vel


func get_pref_velocity() -> Vector2:
	if rvoagent:
		return rvoagent.prefVelocity
	return Vector2(0, 0)

func reset_position(value):
	if rvoagent:
		rvoagent.position = value
		
	position = value
	

