extends Label

var elapsed_time = 0.0

func _ready():
	pass # Replace with function body.

func _update_value():
	self.text = "fps %03d" % Performance.get_monitor(Performance.TIME_FPS)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	elapsed_time += delta
	if elapsed_time > 1:
		elapsed_time = 0
		_update_value()
